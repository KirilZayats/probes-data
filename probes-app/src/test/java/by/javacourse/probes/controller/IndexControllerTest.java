package by.javacourse.probes.controller;

import by.javacourse.probes.model.ResearchObject;
import by.javacourse.probes.service.ResearchObjectService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(MockitoJUnitRunner.class)
public class IndexControllerTest {

    @InjectMocks
    IndexController indexController;

    @Mock
    ResearchObjectService researchObjectService;

    @Mock
    Model model;

    @Test
    public void index() {
        //given
        Set<ResearchObject> researchObjectSet = mock(Set.class);
        when(researchObjectService.findAll()).thenReturn(researchObjectSet);

        //when
        String result = indexController.index(model);

        //then
        assertEquals("index", result);
        verify(researchObjectService, times(1)).findAll();
        verify(model, times(1)).addAttribute(eq("researchObjects"), eq(researchObjectSet));
    }
    @Test
    public void indexWithMockMWC() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(indexController).build();

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }
}