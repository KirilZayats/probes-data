package by.javacourse.probes.controller;

import by.javacourse.probes.model.Planet;
import by.javacourse.probes.service.PlanetService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.Optional;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class PlanetControllerTest {
    @InjectMocks
    PlanetController planetController;

    @Mock
    PlanetService planetService;

    @Mock
    Model model;

    MockMvc mockMvc;

    @Test
    public void processFindPlanets() {
        Planet planet = mock(Planet.class);
        Optional<Planet> planetOptional = mock(Optional.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(planetService.findByName("Name")).thenReturn(planetOptional);
        when(planet.getName()).thenReturn("Name");
        when(planetOptional.isEmpty()).thenReturn(true);

        String result = planetController.processFindPlanets(planet, bindingResult).getViewName();

        assertEquals("planets/findPlanet", result);
        verify(planetService, times(1)).findByName(planet.getName());
        verify(bindingResult, times(1))
                .rejectValue(eq("name"), eq("not found"), eq("not found"));
    }

    @Test
    public void processFindPlanetsElse() {
        Planet planet = mock(Planet.class);
        Optional<Planet> planetOptional = mock(Optional.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(planetService.findByName("Name")).thenReturn(planetOptional);
        when(planet.getName()).thenReturn("Name");
        when(planetOptional.isEmpty()).thenReturn(false);

        String result = planetController.processFindPlanets(planet, bindingResult).getViewName();

        assertEquals("planets/planetDetails", result);
        verify(planetService, times(1)).findByName(planet.getName());

    }


    @BeforeEach
    public void beforeEach() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(planetController).build();
    }


    @Test
    public void showPlanet() throws Exception {
        Planet planet = Planet.builder()
                .id(1234L)
                .build;
        when(planetService.findById(345L)).thenThrow(Exception.class);
        mockMvc.perform(get("/planets/345"))
                .andExpect(status().isOk())
                .andExpect(view().name("planets/planetDetails"))
                .andExpect(model().attribute("planet", hasProperty("id", is(1234L))));

    }

    @Test
    public void processNewPlanet() throws Exception {
        Planet savesPlanet = mock(Planet.class);
        when(savesPlanet.getId()).thenReturn(123L);
        when(planetService.save(any())).thenReturn(savesPlanet);

        mockMvc.perform(post("/planets/new"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/planets/123"));

        verify(planetService, times(1)).save(any());
    }

    @Test
    public void processUpdatePlanet() throws Exception {
        Planet planet = mock(Planet.class);
        Planet savesPlanet = mock(Planet.class);
        when(savesPlanet.getId()).thenReturn(123L);
        when(planetService.save(any())).thenReturn(savesPlanet);

        String result = planetController.processUpdatePlanet(123L, planet);

        //then
        assertEquals("redirect:/owners/123", result);
        verify(planet, times(1)).setId(eq(123L));
        verify(planetService, times(1)).save(eq(planet));

    }
}