package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Equipment;
import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.Probe;
import by.javacourse.probes.repository.ProbeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProbeJpaServiceTest {

    @InjectMocks
    ProbeJpaService probeJpaService;

    @Mock
    ProbeRepository probeRepository;

    @Test
    public void getByEquipmentType() {
        Equipment equipment = Equipment.A_class;
        Probe testProbe = Probe.builder()
                .classOfEquipment(equipment)
                .build();

        when(probeRepository.findAllByClassOfEquipment(equipment)).thenReturn(Set.of(testProbe));

        Set<Probe> result = probeJpaService.getByEquipmentType(equipment);

        assertFalse(result.isEmpty());
        assertEquals(equipment, result.stream().findFirst().get().getClassOfEquipment());
    }

    @Test
    public void getSuitable() {
        String size = "12x3x3/wide";
        Equipment equipment = Equipment.A_class;
        Probe testProbe = Probe.builder()
                .classOfEquipment(equipment)
                .classOfEquipment(equipment)
                .build();

        when(probeRepository.findAllSuitable(size, equipment)).thenReturn(Set.of(testProbe));

        Set<Probe> result = probeJpaService.getSuitable(size, equipment);

        assertFalse(result.isEmpty());
        Probe resultingProbe = result.stream().findFirst().get();
        assertEquals(testProbe, resultingProbe);
    }

    @Test
    public void findAllInGalaxy() {
        Galaxy galaxy = Galaxy.BACKWARD;
        Probe testProbe = Probe.builder()
                .currentLocation("BACKWARD/")
                .build();
        when(probeRepository.findAll()).thenReturn(List.of(testProbe));

        Set<Probe> result = probeJpaService.findAllInGalaxy(Galaxy.BACKWARD);
        assertFalse(result.isEmpty());
        assertEquals(galaxy.name(), result.stream().findFirst().get().getCurrentLocation().split("/")[0]);
    }
}