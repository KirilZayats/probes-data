package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.Planet;
import by.javacourse.probes.model.StarSystem;
import by.javacourse.probes.repository.PlanetRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class PlanetJpaServiceTest {

    @InjectMocks
    PlanetJpaService planetJpaService;

    @Mock
    PlanetRepository planetRepository;

    @Test
    public void sortedByMaxTemperature() {
        int temp1 = 10;
        int temp2 = 2;
        Planet firsPlanet = Planet.builder()
                .temperatureMax(BigDecimal.valueOf(temp1))
                .name("firs")
                .mass(BigDecimal.valueOf(124))
                .build();
        Planet secondPlanet = Planet.builder()
                .temperatureMax(BigDecimal.valueOf(temp2))
                .name("second")
                .mass(BigDecimal.valueOf(111))
                .build();
        when(planetRepository.findAll()).thenReturn(List.of(firsPlanet, secondPlanet));

        List<Planet> result = planetJpaService.sortedByMaxTemperature();

        assertFalse(result.isEmpty());
        assertEquals(BigDecimal.valueOf(temp2), result.get(0).getTemperatureMax());
        assertEquals(BigDecimal.valueOf(temp1), result.get(1).getTemperatureMax());
    }


    @Test
    public void findAllInGalaxy() {
        Planet testPlanet = Planet.builder()
                .system(StarSystem.builder()
                        .galaxy(Galaxy.ANDROMEDA)
                        .build())
                .build();

        when(planetRepository.findAllBySystemGalaxy(Galaxy.ANDROMEDA)).thenReturn(Set.of(testPlanet));
        Set<Planet> planets = planetJpaService.findAllInGalaxy(Galaxy.ANDROMEDA);
        assertEquals(1, planets.size());
        assertEquals(Set.of(testPlanet), planets);
    }

    @Test
    public void getByMaxTemperature() {
        int temp = 10;
        Planet testPlanet = Planet.builder()
                .temperatureMax(BigDecimal.valueOf(temp))
                .build();

        when(planetRepository.getByTemperatureMaxAfter(BigDecimal.valueOf(5))).thenReturn(List.of(testPlanet));

        List<Planet> planets = planetJpaService.getByMaxTemperature(BigDecimal.valueOf(5));
        assertFalse(planets.isEmpty());
        assertEquals(List.of(testPlanet), planets);
    }

    @Test
    public void sortedByMinTemperature() {
        int temp1 = 10;
        int temp2 = 2;
        Planet firsPlanet = Planet.builder()
                .temperatureMin(BigDecimal.valueOf(temp2))
                .name("firs")
                .mass(BigDecimal.valueOf(124))
                .build();
        Planet secondPlanet = Planet.builder()
                .temperatureMin(BigDecimal.valueOf(temp1))
                .name("second")
                .mass(BigDecimal.valueOf(111))
                .build();
        when(planetRepository.findAll()).thenReturn(List.of(firsPlanet, secondPlanet));

        List<Planet> result = planetJpaService.sortedByMinTemperature();

        assertFalse(result.isEmpty());
        assertEquals(BigDecimal.valueOf(temp2), result.get(0).getTemperatureMin());
        assertEquals(BigDecimal.valueOf(temp1), result.get(1).getTemperatureMin());
    }

    @Test
    public void getByMinTemperature() {
        int temp = 2;

        Planet testPlanet = Planet.builder()
                .temperatureMin(BigDecimal.valueOf(temp))
                .build();

        when(planetRepository.getByTemperatureMinBefore(BigDecimal.valueOf(5))).thenReturn(List.of(testPlanet));

        List<Planet> planets = planetJpaService.getByMinTemperature(BigDecimal.valueOf(5));
        assertFalse(planets.isEmpty());
        assertEquals(List.of(testPlanet), planets);
    }

    @Test
    public void getByMaxAverageDistanceToStar() {
        int distant = 10;
        Planet testPlanet = Planet.builder()
                .averageDistanceToStar(BigDecimal.valueOf(distant))
                .build();

        when(planetRepository.getByMaxDistance(BigDecimal.valueOf(5))).thenReturn(List.of(testPlanet));

        List<Planet> planets = planetJpaService.getByMaxAverageDistanceToStar(BigDecimal.valueOf(5));
        assertFalse(planets.isEmpty());
        assertEquals(List.of(testPlanet), planets);
    }

    @Test
    public void getByMinAverageDistanceToStar() {
        int distant = 2;

        Planet testPlanet = Planet.builder()
                .averageDistanceToStar(BigDecimal.valueOf(distant))
                .build();

        when(planetRepository.getByMinDistance(BigDecimal.valueOf(5))).thenReturn(List.of(testPlanet));

        List<Planet> planets = planetJpaService.getByMinAverageDistanceToStar(BigDecimal.valueOf(5));
        assertFalse(planets.isEmpty());
        assertEquals(List.of(testPlanet), planets);
    }

    @Test
    public void getSuitable() {
        BigDecimal tempMax = BigDecimal.valueOf(100);
        BigDecimal tempMin = BigDecimal.valueOf(-12);
        int duration = 24;
        BigDecimal averageDistant = BigDecimal.valueOf(1234);
        BigDecimal period = BigDecimal.valueOf(13);
        Planet testPlanet = Planet.builder()
                .temperatureMin(tempMin)
                .temperatureMax(tempMax)
                .durationOfDay(duration)
                .orbitalPeriod(period)
                .averageDistanceToStar(averageDistant)
                .build();

        when(planetRepository.getAllSuitable(tempMin, tempMin, duration, averageDistant, period)).thenReturn(List.of(testPlanet));

        List<Planet> planets = planetJpaService.getSuitable(tempMin, tempMin, duration, averageDistant, period);
        assertFalse(planets.isEmpty());
        assertEquals(testPlanet, planets.get(0));
    }


}
