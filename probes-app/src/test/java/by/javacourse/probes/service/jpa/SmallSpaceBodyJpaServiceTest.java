package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.SmallSpaceBody;
import by.javacourse.probes.model.StarSystem;
import by.javacourse.probes.repository.SmallSpaceBodyRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class SmallSpaceBodyJpaServiceTest {
    @InjectMocks
    private SmallSpaceBodyJpaService smallSpaceBodyJpaService;

    @Mock
    private SmallSpaceBodyRepository spaceBodyRepository;

    @Test
    public void findByType() {
        String type = "Asteroid";
        SmallSpaceBody smallSpaceBodyTest = SmallSpaceBody.builder()
                .type(type)
                .build();

        when(spaceBodyRepository.findAllByType(type)).thenReturn(Set.of(smallSpaceBodyTest));

        Set<SmallSpaceBody> result = smallSpaceBodyJpaService.findByType(type);

        assertFalse(result.isEmpty());
        assertEquals(type,result.stream().findFirst().get().getType());
    }

    @Test
    public void findAllInGalaxy() {
        Galaxy galaxy = Galaxy.ANDROMEDA;
        SmallSpaceBody smallSpaceBodyTest = SmallSpaceBody.builder()
                .system(StarSystem.builder()
                .galaxy(galaxy)
                .build())
                .build();

        when(spaceBodyRepository.findAllBySystemGalaxy(galaxy)).thenReturn(Set.of(smallSpaceBodyTest));

        Set<SmallSpaceBody> result = smallSpaceBodyJpaService.findAllInGalaxy(galaxy);
        assertFalse(result.isEmpty());
        assertEquals(galaxy,result.stream().findFirst().get().getSystem().getGalaxy());

    }

    @Test
    public void getSuitable() {
        String chemicalComposition = "H1/12U/34Fr";
        SmallSpaceBody smallSpaceBodyTest = SmallSpaceBody.builder()
                .chemicalComposition(chemicalComposition)
                .build();

        when(spaceBodyRepository.findAllSuitable(chemicalComposition)).thenReturn(Set.of(smallSpaceBodyTest));

        Set<SmallSpaceBody> result = smallSpaceBodyJpaService.getSuitable(chemicalComposition);

        assertFalse(result.isEmpty());
        assertEquals(chemicalComposition,result.stream().findFirst().get().getChemicalComposition());
    }
}