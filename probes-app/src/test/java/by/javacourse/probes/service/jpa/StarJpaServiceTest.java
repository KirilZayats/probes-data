package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.SpectralClass;
import by.javacourse.probes.model.Star;
import by.javacourse.probes.repository.StarRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StarJpaServiceTest {

    @InjectMocks
    private StarJpaService starJpaService;

    @Mock
    private StarRepository starRepository;

    @Test
    public void findBySpectrumClass() {
        SpectralClass spectralClass = SpectralClass.A_CLASS;
        Star starTest = Star.builder()
                .spectralClass(spectralClass)
                .build();
        when(starRepository.findAllBySpectralClass(spectralClass)).thenReturn(Set.of(starTest));

        Set<Star> result = starJpaService.findBySpectrumClass(spectralClass);

        assertFalse(result.isEmpty());
        assertEquals(spectralClass, result.stream().findFirst().get().getSpectralClass());
    }

    @Test
    public void getSuitable() {
        SpectralClass spectralClass = SpectralClass.A_CLASS;
        String constellation = "no";

        Star starTest = Star.builder()
                .spectralClass(spectralClass)
                .constellation(constellation)
                .build();

        when(starRepository.findAllSuitable(spectralClass,constellation)).thenReturn(Set.of(starTest));

        Set<Star> result = starJpaService.getSuitable(spectralClass,constellation);

        assertFalse(result.isEmpty());
        Star res = result.stream().findFirst().get();
        assertEquals(spectralClass,res.getSpectralClass());
        assertEquals(constellation,res.getConstellation());
    }
}