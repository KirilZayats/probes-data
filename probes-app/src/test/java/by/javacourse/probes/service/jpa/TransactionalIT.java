package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Planet;
import by.javacourse.probes.repository.PlanetRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionalIT {
    @Autowired
    PlanetJpaService planetJpaService;

    @Autowired
    PlanetRepository planetRepository;


       @Test
    public void updatePlanet() {

        BigDecimal tempMax = BigDecimal.valueOf(100);
        BigDecimal tempMin = BigDecimal.valueOf(-12);
        int duration = 24;
        BigDecimal averageDistant = BigDecimal.valueOf(1234);
        BigDecimal period = BigDecimal.valueOf(13);
        Planet planet = Planet.builder()
                .temperatureMin(tempMin)
                .temperatureMax(tempMax)
                .durationOfDay(duration)
                .orbitalPeriod(period)
                .averageDistanceToStar(averageDistant)
                .build();
        Planet intermediateResult = planetRepository.save(planet);
        assertNotNull(intermediateResult);
        assertNotNull(intermediateResult.getId());
        BigDecimal period2 = BigDecimal.valueOf(29).setScale(2);
        intermediateResult.setOrbitalPeriod(period2);
        planetJpaService.updatePlanet(intermediateResult);
        Planet result = planetJpaService.findById(intermediateResult.getId());
        assertEquals(period2, result.getOrbitalPeriod());
    }
}
