package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.ResearchObject;
import by.javacourse.probes.repository.ResearchObjectRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResearchObjectJpaServiceTest {
    @InjectMocks
    private ResearchObjectJpaService researchObjectJpaService;
    @Mock
    private ResearchObjectRepository researchObjectRepository;

    @Test
    public void findAllInGalaxy() {
        Galaxy galaxy = Galaxy.ANDROMEDA;
        ResearchObject researchObjectTest = ResearchObject.builder()
                .galaxy(galaxy)
                .build();
        when(researchObjectRepository.findAllByGalaxy(galaxy)).thenReturn(Set.of(researchObjectTest));

        Set<ResearchObject> result = researchObjectJpaService.findAllInGalaxy(galaxy);

        assertFalse(result.isEmpty());
        assertEquals(galaxy,result.stream().findFirst().get().getGalaxy());
    }
}