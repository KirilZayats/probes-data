package by.javacourse.probes.service;

import by.javacourse.probes.model.ResearchObject;

public interface ResearchObjectService extends CrudService<ResearchObject, Long> {
}
