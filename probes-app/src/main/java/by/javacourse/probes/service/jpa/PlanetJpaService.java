package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.Planet;
import by.javacourse.probes.repository.PlanetRepository;
import by.javacourse.probes.service.PlanetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Transactional(readOnly = true)
@Service
public class PlanetJpaService extends AbstractJpaService<Planet, Long> implements PlanetService {
    @Autowired
    private PlanetRepository planetRepository;

    @Override
    public JpaRepository<Planet, Long> getRepository() {
        return planetRepository;
    }

    @Override
    public List<Planet> sortedByMaxTemperature() {
        return planetRepository.findAll().stream()
                .sorted(Comparator.comparing(Planet::getTemperatureMax)).collect(Collectors.toList());
    }

    @Override
    public Set<Planet> findAllInGalaxy(Galaxy galaxy) {
        return planetRepository.findAllBySystemGalaxy(galaxy);
    }

    @Override
    public List<Planet> getByMaxTemperature(BigDecimal max) {
        return planetRepository.getByTemperatureMaxAfter(max);
    }

    @Override
    public List<Planet> sortedByMinTemperature() {
        return planetRepository.findAll().stream()
                .sorted(Comparator.comparing(Planet::getTemperatureMin)).collect(Collectors.toList());
    }

    @Override
    public List<Planet> getByMinTemperature(BigDecimal min) {
        return planetRepository.getByTemperatureMinBefore(min);
    }

    @Override
    public List<Planet> getByMaxAverageDistanceToStar(BigDecimal max) {
        return planetRepository.getByMaxDistance(max);
    }

    @Override
    public List<Planet> getByMinAverageDistanceToStar(BigDecimal min) {
        return planetRepository.getByMinDistance(min);
    }

    @Override
    public List<Planet> getSuitable(BigDecimal temperatureMin, BigDecimal temperatureMax, int durationOfDay,
                                    BigDecimal averageDistanceToStar, BigDecimal orbitalPeriod) {
        return planetRepository.getAllSuitable(temperatureMin, temperatureMax, durationOfDay,
                averageDistanceToStar, orbitalPeriod);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
    public void updatePlanet(Planet planet) {
        Planet planetFromDB = planetRepository.findById(planet.getId()).orElseThrow();
        planetFromDB.setAverageDistanceToStar(planet.getAverageDistanceToStar());
        planetFromDB.setOrbitalPeriod(planet.getOrbitalPeriod());
        planetFromDB.setTemperatureMax(planet.getTemperatureMax());
        planetFromDB.setTemperatureMin(planet.getTemperatureMin());
    }
}
