package by.javacourse.probes.service;

import by.javacourse.probes.model.Planet;

import java.math.BigDecimal;
import java.util.List;

public interface PlanetService extends CrudService<Planet, Long> {
    List<Planet> sortedByMaxTemperature();

    List<Planet> getByMaxTemperature(BigDecimal max);

    List<Planet> sortedByMinTemperature();

    List<Planet> getByMinTemperature(BigDecimal min);

    List<Planet> getByMaxAverageDistanceToStar(BigDecimal max);

    List<Planet> getByMinAverageDistanceToStar(BigDecimal min);

    List<Planet> getSuitable(BigDecimal temperatureMin, BigDecimal temperatureMax,
                             int durationOfDay, BigDecimal AverageDistanceToStar,
                             BigDecimal orbitalPeriod);
}
