package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Equipment;
import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.Probe;
import by.javacourse.probes.repository.ProbeRepository;
import by.javacourse.probes.service.ProbeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Transactional(readOnly = true)
@Service
public class ProbeJpaService extends AbstractJpaService<Probe, String> implements ProbeService {

    @Autowired
    private ProbeRepository probeRepository;

    @Autowired
    private ResearchObjectJpaService researchObjectJpaService;


    public JpaRepository<Probe, String> getRepository() {
        return probeRepository;
    }

    @Override
    public Set<Probe> getByEquipmentType(Equipment type) {
        return probeRepository.findAllByClassOfEquipment(type);
    }

    @Override
    public Set<Probe> getSuitable(String size, Equipment classOfEquipment) {
        return probeRepository.findAllSuitable(size, classOfEquipment);
    }

    @Override
    public Set<Probe> findAllInGalaxy(Galaxy galaxy) {
        return probeRepository.findAll().stream()
                .filter(o -> o.getCurrentLocation().split("/")[0]
                        .equalsIgnoreCase(galaxy.name())).collect(Collectors.toSet());
    }


    @Transactional
    public void clean(OffsetDateTime time) {
        probeRepository.deleteLongInactiveProbes(time);

    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void update(Probe probe) {
        Probe probeFromDB = probeRepository.findById(probe.getId()).orElseThrow();
        probeFromDB.getResearchObjects().addAll(probe.getResearchObjects());
        researchObjectJpaService.updateResearchObject(probe.getResearchObjects()
                .stream().filter(o -> o.getDataUpdateTime().compareTo(probe.getLastContact()) == 0)
                .collect(Collectors.toList()).get(0));
    }

}
