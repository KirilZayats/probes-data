package by.javacourse.probes.service;

import by.javacourse.probes.model.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrudJpaService<T extends BaseEntity<ID>, ID> extends CrudService<T, ID> {
    JpaRepository<T, ID> getRepository();
}
