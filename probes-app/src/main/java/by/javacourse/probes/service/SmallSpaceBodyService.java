package by.javacourse.probes.service;

import by.javacourse.probes.model.SmallSpaceBody;

import java.util.Set;

public interface SmallSpaceBodyService extends CrudService<SmallSpaceBody, Long> {
    Set<SmallSpaceBody> findByType(String type);

    Set<SmallSpaceBody> getSuitable(String chemicalComposition);
}
