package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.SmallSpaceBody;
import by.javacourse.probes.repository.SmallSpaceBodyRepository;
import by.javacourse.probes.service.SmallSpaceBodyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Transactional(readOnly = true)
@Service
public class SmallSpaceBodyJpaService extends AbstractJpaService<SmallSpaceBody, Long> implements SmallSpaceBodyService {
    @Autowired
    private SmallSpaceBodyRepository spaceBodyRepository;

    @Override
    public JpaRepository<SmallSpaceBody, Long> getRepository() {
        return spaceBodyRepository;
    }

    @Override
    public Set<SmallSpaceBody> findByType(String type) {
        return spaceBodyRepository.findAllByType(type);
    }

    @Override
    public Set<SmallSpaceBody> findAllInGalaxy(Galaxy galaxy) {
        return spaceBodyRepository.findAllBySystemGalaxy(galaxy);
    }

    @Override
    public Set<SmallSpaceBody> getSuitable(String chemicalComposition) {
        return spaceBodyRepository.findAllSuitable(chemicalComposition);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
    public void update(SmallSpaceBody smallSpaceBody) {
        SmallSpaceBody smallSpaceBodyFromDB = spaceBodyRepository.findById(smallSpaceBody.getId()).orElseThrow();
        smallSpaceBodyFromDB.setSpeed(smallSpaceBody.getSpeed());
        smallSpaceBodyFromDB.setChemicalComposition(smallSpaceBody.getChemicalComposition());
        smallSpaceBodyFromDB.setType(smallSpaceBody.getType());
    }
}
