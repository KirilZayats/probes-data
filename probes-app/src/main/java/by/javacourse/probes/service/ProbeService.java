package by.javacourse.probes.service;


import by.javacourse.probes.model.Equipment;
import by.javacourse.probes.model.Probe;

import java.util.Set;


public interface ProbeService extends CrudService<Probe, String> {
    Set<Probe> getByEquipmentType(Equipment type);

    Set<Probe> getSuitable(String size, Equipment classOfEquipment);
}
