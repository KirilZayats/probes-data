package by.javacourse.probes.service;


import by.javacourse.probes.model.Galaxy;

import java.util.Optional;
import java.util.Set;

public interface CrudService<T, ID> {

    Optional<T> findByName(String name);

    T findById(ID id);

    T save(T object);

    Set<T> findAllInGalaxy(Galaxy galaxy);

    Set<T> findAll();

    void delete(T object);

    void deleteById(ID id);

}
