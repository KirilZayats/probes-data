package by.javacourse.probes.service;

import by.javacourse.probes.model.StarSystem;
import by.javacourse.probes.model.StarSystemType;

import java.util.Set;

public interface StarSystemService extends CrudService<StarSystem, Long> {
    Set<StarSystem> findByType(StarSystemType starSystemType);
}
