package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.BaseEntity;
import by.javacourse.probes.service.CrudJpaService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public abstract class AbstractJpaService<T extends BaseEntity<ID>, ID> implements CrudJpaService<T, ID> {

    @Override
    public Optional<T> findByName(String name) {
        return getRepository().findAll().stream().filter(o -> o.getName().equalsIgnoreCase(name)).findFirst();
    }

    @Override
    public T findById(ID id) {
        Optional<T> object = getRepository().findById(id);
        if (object.isEmpty()) {
            throw new RuntimeException();
        }
        return object.get();

    }

    @Override
    public T save(T object) {
        return getRepository().save(object);
    }


    @Override
    public Set<T> findAll() {
        return new HashSet<>(getRepository().findAll());
    }

    @Override
    public void delete(T object) {
        getRepository().delete(object);
    }

    @Override
    public void deleteById(ID id) {
        getRepository().deleteById(id);
    }
}
