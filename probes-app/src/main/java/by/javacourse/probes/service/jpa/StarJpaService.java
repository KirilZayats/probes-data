package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.SpectralClass;
import by.javacourse.probes.model.Star;
import by.javacourse.probes.repository.StarRepository;
import by.javacourse.probes.service.StarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Transactional(readOnly = true)
@Service
public class StarJpaService extends AbstractJpaService<Star, Long> implements StarService {
    @Autowired
    private StarRepository starRepository;

    @Override
    public JpaRepository<Star, Long> getRepository() {
        return starRepository;
    }

    @Override
    public Set<Star> findBySpectrumClass(SpectralClass spectralClass) {
        return starRepository.findAllBySpectralClass(spectralClass);
    }

    @Override
    public Set<Star> getSuitable(SpectralClass spectralClass, String constellation) {
        return starRepository.findAllSuitable(spectralClass, constellation);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
    public void update(Star star) {
        Star starFromDB = starRepository.findById(star.getId()).orElseThrow();
        starFromDB.setConstellation(star.getConstellation());
        starFromDB.setTemperature(star.getTemperature());
        starFromDB.setSpectrumFeatures(star.getSpectrumFeatures());
        starFromDB.setSpectralClass(star.getSpectralClass());
    }

    @Override
    public Set<Star> findAllInGalaxy(Galaxy galaxy) {
        return starRepository.findAllBySystemGalaxy(galaxy);
    }
}
