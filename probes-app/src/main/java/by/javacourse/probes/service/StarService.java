package by.javacourse.probes.service;

import by.javacourse.probes.model.SpectralClass;
import by.javacourse.probes.model.Star;

import java.util.Set;

public interface StarService extends CrudService<Star, Long> {
    Set<Star> findBySpectrumClass(SpectralClass spectralClass);

    Set<Star> getSuitable(SpectralClass spectralClass, String constellation);

}
