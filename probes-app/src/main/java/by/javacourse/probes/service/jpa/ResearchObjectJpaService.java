package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.ResearchObject;
import by.javacourse.probes.repository.ResearchObjectRepository;
import by.javacourse.probes.service.ResearchObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Set;

@Transactional(readOnly = true)
@Service
public class ResearchObjectJpaService extends AbstractJpaService<ResearchObject, Long> implements ResearchObjectService {
    @Autowired
    private ResearchObjectRepository researchObjectRepository;

    public JpaRepository<ResearchObject, Long> getRepository() {
        return researchObjectRepository;
    }

    @Override
    public Set<ResearchObject> findAllInGalaxy(Galaxy galaxy) {
        return researchObjectRepository.findAllByGalaxy(galaxy);
    }


    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
    public void updateResearchObject(ResearchObject researchObject) {
        ResearchObject researchObjectFromDB = researchObjectRepository.findById(researchObject.getId()).orElseThrow();
        researchObjectFromDB.setNumberOfResearchProbes(researchObject.getNumberOfResearchProbes());
    }

    @Transactional
    public void clear(OffsetDateTime time) {
        researchObjectRepository.deleteOld(time);
    }
}
