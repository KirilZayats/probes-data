package by.javacourse.probes.service.jpa;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.StarSystem;
import by.javacourse.probes.model.StarSystemType;
import by.javacourse.probes.repository.StarSystemRepository;
import by.javacourse.probes.service.StarSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;


@Transactional(readOnly = true)
@Service
public class StarSystemJpaService extends AbstractJpaService<StarSystem, Long> implements StarSystemService {
    @Autowired
    private StarSystemRepository systemRepository;

    @Override
    public JpaRepository<StarSystem, Long> getRepository() {
        return systemRepository;
    }


    @Override
    public Set<StarSystem> findAllInGalaxy(Galaxy galaxy) {
        return systemRepository.findAllByGalaxy(galaxy);
    }

    @Override
    public Set<StarSystem> findByType(StarSystemType starSystemType) {
        return systemRepository.findAllByType(starSystemType);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
    public void update(StarSystem starSystem) {
        StarSystem systemFromDB = systemRepository.findById(starSystem.getId()).orElseThrow();
        systemFromDB.setType(starSystem.getType());
        systemFromDB.getPlanets().addAll(starSystem.getPlanets());
        systemFromDB.getSmallSpaceBodies().addAll(starSystem.getSmallSpaceBodies());
        systemFromDB.getStars().addAll(starSystem.getStars());
    }


}
