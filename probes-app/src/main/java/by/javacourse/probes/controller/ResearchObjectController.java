package by.javacourse.probes.controller;


import by.javacourse.probes.model.ResearchObject;
import by.javacourse.probes.service.ResearchObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@RequestMapping("/res_objects")
@Controller
public class ResearchObjectController {

    @Autowired
    private ResearchObjectService researchObjectService;

    @GetMapping("/index")
    public String getResObj(Model model) {
        model.addAttribute("researchObjects", researchObjectService.findAll());
        return "research_objects/index";
    }

    @GetMapping("/find")
    public String findResObj(Model model) {
        model.addAttribute("resObj", ResearchObject.builder().build());
        return "research_objects/findResObj";
    }

    @GetMapping
    public ModelAndView processFindResObj(ResearchObject object, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<ResearchObject> researchObject = researchObjectService.findByName(object.getName());
        if (researchObject.isEmpty()) {
            bindingResult.rejectValue("name", "not found", "not found");
            modelAndView.setViewName("research_objects/findResObj");
        } else {
            modelAndView.addObject("researchObject", researchObject.get());
            modelAndView.setViewName("research_objects/researchObjectDetails");
        }
        return modelAndView;

    }

    @GetMapping("/{id}")
    public ModelAndView showResObj(@PathVariable(name = "id") Long resObjId) {
        ModelAndView modelAndView = new ModelAndView("research_objects/researchObjectDetails");
        ResearchObject researchObject = researchObjectService.findById(resObjId);
        modelAndView.addObject("researchObject", researchObject);
        return modelAndView;
    }

    @GetMapping("/new")
    public String newStar(Model model) {
        model.addAttribute("resObj", ResearchObject.builder().build());
        return "research_objects/createOrUpdateResObjForm";
    }

    @PostMapping("/new")
    public String processNewStar(ResearchObject researchObject) {
        ResearchObject researchObject1 = researchObjectService.save(researchObject);
        return "redirect:/res_objects/" + researchObject1.getId();
    }

    @GetMapping("/{resObjId}/edit")
    public String updateStar(@PathVariable Long resObjId, Model model) {
        model.addAttribute("resObj", researchObjectService.findById(resObjId));
        return "research_objects/createOrUpdateResObjForm";
    }

    @PostMapping("/{resObjId}/edit")
    public String processUpdateStar(@PathVariable Long resObjId, ResearchObject researchObject) {
        researchObject.setId(resObjId);
        ResearchObject researchObject1 = researchObjectService.save(researchObject);
        return "redirect:/res_objects/" + researchObject1.getId();
    }

}