package by.javacourse.probes.controller;

import by.javacourse.probes.model.SmallSpaceBody;
import by.javacourse.probes.service.SmallSpaceBodyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@RequestMapping("/ss_bodies")
@Controller
public class SmallSpaceBodyController {

    @Autowired
    private SmallSpaceBodyService smallSpaceBodyService;

    @GetMapping("/index")
    public String getProbes(Model model) {
        model.addAttribute("smallSpaceBodies", smallSpaceBodyService.findAll());
        return "small_space_bodies/index";
    }

    @GetMapping("/find")
    public String findPlanets(Model model) {
        model.addAttribute("sSBody", SmallSpaceBody.builder().build());
        return "small_space_bodies/findSSBody";
    }

    @GetMapping
    public ModelAndView processFindPlanets(SmallSpaceBody smallSpaceBody, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<SmallSpaceBody> smallSpaceBodyOptional = smallSpaceBodyService.findByName(smallSpaceBody.getName());
        if (smallSpaceBodyOptional.isEmpty()) {
            bindingResult.rejectValue("name", "not found", "not found");
            modelAndView.setViewName("small_space_bodies/findSSBody");
        } else {
            modelAndView.addObject("sSBody", smallSpaceBodyOptional.get());
            modelAndView.setViewName("small_space_bodies/smallSpaceBodyDetails");
        }
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView showSSBody(@PathVariable(name = "id") Long sSBodyId) {
        ModelAndView modelAndView = new ModelAndView("small_space_bodies/smallSpaceBodyDetails");
        SmallSpaceBody smallSpaceBody = smallSpaceBodyService.findById(sSBodyId);
        modelAndView.addObject("sSBody", smallSpaceBody);
        return modelAndView;
    }

    @GetMapping("/new")
    public String newSSBody(Model model) {
        model.addAttribute("sSBody", SmallSpaceBody.builder().build());
        return "small_space_bodies/createOrUpdateSSBodiesForm";
    }

    @PostMapping("/new")
    public String processNewSSBody(SmallSpaceBody smallSpaceBody) {
        SmallSpaceBody smallSpaceBody1 = smallSpaceBodyService.save(smallSpaceBody);
        return "redirect:/ss_bodies/" + smallSpaceBody1.getId();
    }

    @GetMapping("/{sSBodyId}/edit")
    public String updateSSBody(@PathVariable Long sSBodyId, Model model) {
        model.addAttribute("sSBody", smallSpaceBodyService.findById(sSBodyId));
        return "small_space_bodies/createOrUpdateSSBodiesForm";
    }

    @PostMapping("/{sSBodyId}/edit")
    public String processUpdateSSBody(@PathVariable Long sSBodyId, SmallSpaceBody smallSpaceBody) {
        smallSpaceBody.setId(sSBodyId);
        SmallSpaceBody smallSpaceBody1 = smallSpaceBodyService.save(smallSpaceBody);
        return "redirect:/ss_bodies/" + smallSpaceBody1.getId();
    }
}
