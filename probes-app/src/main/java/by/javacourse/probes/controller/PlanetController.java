package by.javacourse.probes.controller;

import by.javacourse.probes.model.Planet;
import by.javacourse.probes.service.PlanetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;


@RequestMapping("/planets")
@Controller
public class PlanetController {
    @Autowired
    private PlanetService planetService;

    @GetMapping("/index")
    public String getProbes(Model model) {
        model.addAttribute("planets", planetService.findAll());
        return "planets/index";
    }

    @GetMapping("/find")
    public String findPlanets(Model model) {
        model.addAttribute("planet", Planet.builder().build());
        return "planets/findPlanet";
    }

    @GetMapping
    public ModelAndView processFindPlanets(Planet planet, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Planet> planetOptional = planetService.findByName(planet.getName());
        if (planetOptional.isEmpty()) {
            bindingResult.rejectValue("name", "not found", "not found");
            modelAndView.setViewName("planets/findPlanet");
        } else {
            modelAndView.addObject("planet", planetOptional.get());
            modelAndView.setViewName("planets/planetDetails");
        }
        return modelAndView;


    }

    @GetMapping("/{id}")
    public ModelAndView showPlanet(@PathVariable(name = "id") Long planetId) {
        ModelAndView modelAndView = new ModelAndView("planets/planetDetails");
        Planet planet = planetService.findById(planetId);
        modelAndView.addObject("planet", planet);
        return modelAndView;
    }

    @GetMapping("/new")
    public String newPlanet(Model model) {
        model.addAttribute("planet", Planet.builder().build());
        return "planets/createOrUpdatePlanetForm";
    }

    @PostMapping("/new")
    public String processNewPlanet(Planet planet) {
        Planet savedPlanet = planetService.save(planet);
        return "redirect:/planets/" + savedPlanet.getId();
    }

    @GetMapping("/{planetId}/edit")
    public String updatePlanet(@PathVariable Long planetId, Model model) {
        model.addAttribute("planet", planetService.findById(planetId));
        return "planets/createOrUpdatePlanetForm";
    }

    @PostMapping("/{planetId}/edit")
    public String processUpdatePlanet(@PathVariable Long planetId, Planet planet) {
        planet.setId(planetId);
        Planet savedPlanet = planetService.save(planet);
        return "redirect:/planets/" + savedPlanet.getId();
    }

}
