package by.javacourse.probes.controller;

import by.javacourse.probes.model.ResearchObject;
import by.javacourse.probes.service.ResearchObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Set;

@Controller
public class IndexController {
    @Autowired
    private ResearchObjectService researchObjectService;

    @RequestMapping(value = {"", "/", "index", "index.html"})
    public String index(Model model) {
        Set<ResearchObject> researchObjectSet =researchObjectService.findAll();
        model.addAttribute("researchObjects",researchObjectSet);
        return "index";
    }
}
