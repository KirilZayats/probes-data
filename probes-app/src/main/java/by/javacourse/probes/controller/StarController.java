package by.javacourse.probes.controller;


import by.javacourse.probes.model.Star;
import by.javacourse.probes.service.StarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@RequestMapping("/stars")
@Controller
public class StarController {
    @Autowired
    private StarService starService;

    @GetMapping("/index")
    public String getStar(Model model) {
        model.addAttribute("stars", starService.findAll());
        return "stars/index";
    }

    @GetMapping("/find")
    public String findStar(Model model) {
        model.addAttribute("star", Star.builder().build());
        return "stars/findStar";
    }

    @GetMapping
    public ModelAndView processFindStars(Star star, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Star> starOptional = starService.findByName(star.getName());
        if (starOptional.isEmpty()) {
            bindingResult.rejectValue("name", "not found", "not found");
            modelAndView.setViewName("stars/findStar");
        } else {
            modelAndView.addObject("star", starOptional.get());
            modelAndView.setViewName("stars/starDetails");
        }
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView showStar(@PathVariable(name = "id") Long starId) {
        ModelAndView modelAndView = new ModelAndView("stars/starDetails");
        Star star = starService.findById(starId);
        modelAndView.addObject("star", star);
        return modelAndView;
    }

    @GetMapping("/new")
    public String newStar(Model model) {
        model.addAttribute("star", Star.builder().build());
        return "stars/createOrUpdateStarForm";
    }

    @PostMapping("/new")
    public String processNewStar(Star star) {
        Star savedStar = starService.save(star);
        return "redirect:/stars/" + savedStar.getId();
    }

    @GetMapping("/{starId}/edit")
    public String updateStar(@PathVariable Long starId, Model model) {
        model.addAttribute("star", starService.findById(starId));
        return "stars/createOrUpdateStarForm";
    }

    @PostMapping("/{starId}/edit")
    public String processUpdateStar(@PathVariable Long starId, Star star) {
        star.setId(starId);
        Star savedStar = starService.save(star);
        return "redirect:/stars/" + savedStar.getId();
    }
}
