package by.javacourse.probes.controller;

import by.javacourse.probes.model.Planet;
import by.javacourse.probes.model.Probe;
import by.javacourse.probes.service.ProbeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@RequestMapping("/probes")
@Controller
public class ProbeController {
    @Autowired
    private ProbeService probeService;

    @GetMapping("/index")
    public String getProbes(Model model) {
        model.addAttribute("probes", probeService.findAll());
        return "probes/index";
    }

    @GetMapping("/find")
    public String findProbe(Model model) {
        model.addAttribute("probe", Probe.builder().build());
        return "probes/findProbe";
    }

    @GetMapping
    public ModelAndView processFindProbe(Probe probe, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Probe> probeOptional = probeService.findByName(probe.getName());
        if (probeOptional.isEmpty()) {
            bindingResult.rejectValue("name", "not found", "not found");
            modelAndView.setViewName("probes/findProbe");
        } else {
            modelAndView.addObject("probe", probeOptional.get());
            modelAndView.setViewName("probes/probeDetails");
        }
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView showProbe(@PathVariable(name = "id") String probeId) {
        ModelAndView modelAndView = new ModelAndView("probes/probeDetails");
        Probe probe = probeService.findById(probeId);
        modelAndView.addObject("probe", probe);
        return modelAndView;
    }

    @GetMapping("/new")
    public String newProbe(Model model) {
        model.addAttribute("probe", Probe.builder().build());
        return "probes/createOrUpdateProbeForm";
    }

    @PostMapping("/new")
    public String processNewProbe(Probe probe) {
        Probe probe1 = probeService.save(probe);
        return "redirect:/probes/" + probe1.getId();
    }

    @GetMapping("/{probeId}/edit")
    public String updateProbe(@PathVariable String probeId, Model model) {
        model.addAttribute("probe", probeService.findById(probeId));
        return "probes/createOrUpdateProbeForm";
    }

    @PostMapping("/{probeId}/edit")
    public String processUpdateProbe(@PathVariable String probeId, Probe probe) {
        probe.setId(probeId);
        Probe probe1 = probeService.save(probe);
        return "redirect:/probes/" + probe1.getId();
    }
}
