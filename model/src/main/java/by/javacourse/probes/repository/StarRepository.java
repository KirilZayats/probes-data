package by.javacourse.probes.repository;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.SpectralClass;
import by.javacourse.probes.model.Star;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface StarRepository extends JpaRepository<Star, Long> {
    Set<Star> findAllBySpectralClass(SpectralClass spectralClass);

    @Query(value = "SELECT o from Star o where o.constellation =:constellation and  o.spectralClass =:spectrum")
    Set<Star> findAllSuitable(@Param("spectrum") SpectralClass spectralClass, @Param("constellation") String constellation);

    Set<Star> findAllBySystemGalaxy(Galaxy galaxy);
}
