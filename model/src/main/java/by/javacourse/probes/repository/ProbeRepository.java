package by.javacourse.probes.repository;

import by.javacourse.probes.model.Equipment;
import by.javacourse.probes.model.Probe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.OffsetDateTime;
import java.util.Set;

public interface ProbeRepository extends JpaRepository<Probe, String> {

    @Modifying
    @Query(value = "DELETE from Probe o where o.lastContact>=:lastContact")
    void deleteLongInactiveProbes(@Param("lastContact") OffsetDateTime time);

    Set<Probe> findAllByClassOfEquipment(Equipment equipment);

    @Query(value = "SELECT o from Probe o where o.classOfEquipment =:equipment and o.size =:size")
    Set<Probe> findAllSuitable(@Param("size") String size, @Param("equipment") Equipment equipment);


}
