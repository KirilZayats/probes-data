package by.javacourse.probes.repository;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.StarSystem;
import by.javacourse.probes.model.StarSystemType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface StarSystemRepository extends JpaRepository<StarSystem, Long> {
    Set<StarSystem> findAllByType(StarSystemType starSystemType);

    Set<StarSystem> findAllByGalaxy(Galaxy galaxy);
}
