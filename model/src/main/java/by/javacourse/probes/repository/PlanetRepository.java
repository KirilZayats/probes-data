package by.javacourse.probes.repository;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.Planet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public interface PlanetRepository extends JpaRepository<Planet, Long> {
    List<Planet> getByTemperatureMaxAfter(BigDecimal max);

    List<Planet> getByTemperatureMinBefore(BigDecimal min);

    @Query(value = "SELECT o from Planet o where o.averageDistanceToStar>=:MaxDist")
    List<Planet> getByMaxDistance(@Param("MaxDist") BigDecimal distance);

    @Query(value = "SELECT o from Planet o where o.averageDistanceToStar<=:MinDist")
    List<Planet> getByMinDistance(@Param("MinDist") BigDecimal distance);

    @Query(value = "SELECT o from Planet o where o.temperatureMax =:MaxT and o.temperatureMin =:MinT " +
            "and o.durationOfDay=:duration and o.averageDistanceToStar =:averageDist and o.orbitalPeriod =:period")
    List<Planet> getAllSuitable(@Param("MaxT") BigDecimal temperatureMax, @Param("MinT") BigDecimal temperatureMin,
                                @Param("duration") Integer durationOfDay,
                                @Param("averageDist") BigDecimal averageDistanceToStar, @Param("period") BigDecimal period);

    Set<Planet> findAllBySystemGalaxy(Galaxy galaxy);
}
