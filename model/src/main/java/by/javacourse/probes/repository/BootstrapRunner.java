package by.javacourse.probes.repository;

import by.javacourse.probes.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.awt.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Set;

@Transactional
@RequiredArgsConstructor
@Component
public class BootstrapRunner implements CommandLineRunner {
    private final ResearchObjectRepository researchObjectRepository;
    private final StarSystemRepository starSystemRepository;

    @Override
    public void run(String... args) {
        Planet planet1 = Planet.builder()
                .name("Venera")
                .averageDistanceToStar(BigDecimal.valueOf(1234))
                .diameter(BigDecimal.valueOf(43))
                .durationOfDay(13)
                .mass(BigDecimal.valueOf(12))
                .numberOfSatellites(0)
                .temperatureMax(BigDecimal.valueOf(1000))
                .temperatureMin(BigDecimal.valueOf(-26))
                .orbitalPeriod(BigDecimal.valueOf(125))
                .build();
        Star star1 = Star.builder()
                .name("Sun")
                .color(Color.YELLOW)
                .diameter(BigDecimal.valueOf(1400))
                .spectralClass(SpectralClass.G_CLASS)
                .mass(BigDecimal.valueOf(2000))
                .constellation("no")
                .temperature("5000")
                .build();
        ProbeEquipment probeEquipment = ProbeEquipment.builder()
                .name("rad_sonar/L-381X12")
                .classType(Equipment.A_class)
                .build();
        Probe probe1 = Probe.builder()
                .name("SH-1n8")
                .id("Some id")
                .size("12x3x3/wide")
                .equipment(Set.of(probeEquipment))
                .classOfEquipment(Equipment.B_class)
                .lastContact(OffsetDateTime.now())
                .build();
        probeEquipment.setProbe(probe1);
        StarSystem system = StarSystem.builder()
                .name("Some system")
                .galaxy(Galaxy.ANDROMEDA)
                .size(BigDecimal.valueOf(1234523))
                .type(StarSystemType.UNARY)
                .planets(Set.of(planet1))
                .stars(Set.of(star1))
                .build();
        planet1.setSystem(system);
        star1.setSystem(system);
        ResearchObject researchObject = ResearchObject.builder()
                .name(planet1.getName())
                .typeOfSpaceObject(TypeOfSpaceObject.PLANET)
                .numberOfResearchProbes(1)
                .galaxy(Galaxy.ANDROMEDA)
                .dataUpdateTime(OffsetDateTime.now())
                .lastDataUpdateProbes(Set.of(probe1))
                .build();
        probe1.setResearchObjects(Set.of(researchObject));
        SmallSpaceBody smallSpaceBody = SmallSpaceBody.builder()
                .name("Rex-12/23")
                .diameter(BigDecimal.valueOf(12))
                .type("Asteroid")
                .speed(BigDecimal.valueOf(2345))
                .system(system)
                .chemicalComposition("12ox/34c/98ph")
                .build();
        system.setSmallSpaceBodies(Set.of(smallSpaceBody));
        starSystemRepository.save(system);
        researchObjectRepository.save(researchObject);

    }
}
