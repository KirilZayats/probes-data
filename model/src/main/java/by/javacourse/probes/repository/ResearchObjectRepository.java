package by.javacourse.probes.repository;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.ResearchObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.OffsetDateTime;
import java.util.Set;

public interface ResearchObjectRepository extends JpaRepository<ResearchObject, Long> {

    @Modifying
    @Query("DELETE from ResearchObject o where o.dataUpdateTime>=:dataUpdateTime")
    void deleteOld(@Param("dataUpdateTime") OffsetDateTime time);

    Set<ResearchObject> findAllByGalaxy(Galaxy galaxy);
}
