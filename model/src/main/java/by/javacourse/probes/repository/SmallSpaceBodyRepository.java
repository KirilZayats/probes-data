package by.javacourse.probes.repository;

import by.javacourse.probes.model.Galaxy;
import by.javacourse.probes.model.SmallSpaceBody;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface SmallSpaceBodyRepository extends JpaRepository<SmallSpaceBody, Long> {
    Set<SmallSpaceBody> findAllByType(String type);

    Set<SmallSpaceBody> findAllBySystemGalaxy(Galaxy galaxy);

    @Query(value = "select o from  SmallSpaceBody o where o.chemicalComposition = :composition")
    Set<SmallSpaceBody> findAllSuitable(@Param("composition") String composition);
}
