package by.javacourse.probes.model;

public enum TypeOfSpaceObject {
    PLANET,
    STAR,
    SMALL_BODY
}
