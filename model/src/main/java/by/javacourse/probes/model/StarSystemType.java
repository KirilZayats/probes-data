package by.javacourse.probes.model;

public enum StarSystemType {
    UNARY,
    BINARY,
    TRINARY,
    QUATERNARY,
    QUINTENARY,
    SEXTENARY,
    SEPTENARY
}
