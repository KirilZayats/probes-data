package by.javacourse.probes.model;


import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
@SuperBuilder
@AllArgsConstructor
public class BaseEntity<ID> {
    public ID getId() {
        return null;
    }

    public String getName() {
        return null;
    }


}
