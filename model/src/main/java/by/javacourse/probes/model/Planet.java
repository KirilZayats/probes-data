package by.javacourse.probes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import java.math.BigDecimal;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@Entity
public class Planet extends SpaceObject {

    private BigDecimal temperatureMin;
    private BigDecimal temperatureMax;
    private Integer durationOfDay;
    private BigDecimal averageDistanceToStar;
    private BigDecimal orbitalPeriod;
    private Integer numberOfSatellites;


    @PrePersist
    private void setType() {
        this.objectType = TypeOfSpaceObject.PLANET;
    }
}