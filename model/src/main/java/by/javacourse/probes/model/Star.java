package by.javacourse.probes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import java.awt.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@Entity
public class Star extends SpaceObject {
    @Enumerated(EnumType.STRING)
    private SpectralClass spectralClass;
    private Color color;
    private String temperature;
    private String spectrumFeatures;
    private String constellation;

    @PrePersist
    private void setType() {
        this.objectType = TypeOfSpaceObject.STAR;
    }
}
