package by.javacourse.probes.model;

public enum Galaxy {
    ANDROMEDA,
    ANTENNAE,
    BACKWARD,
    BLACK_EYE,
    BODES
}
