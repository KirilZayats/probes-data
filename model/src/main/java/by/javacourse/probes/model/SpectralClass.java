package by.javacourse.probes.model;

public enum SpectralClass {
    O_CLASS,
    B_CLASS,
    A_CLASS,
    F_CLASS,
    G_CLASS,
    K_CLASS,
    M_CLASS,
    L_CLASS
}
