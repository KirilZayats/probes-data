package by.javacourse.probes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import java.math.BigDecimal;

@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class SmallSpaceBody extends SpaceObject {
    BigDecimal speed;
    private String type;
    private String chemicalComposition;

    @PrePersist
    private void setType() {
        this.objectType = TypeOfSpaceObject.SMALL_BODY;
    }

}
