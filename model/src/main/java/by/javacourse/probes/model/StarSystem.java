package by.javacourse.probes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@Entity
public class StarSystem extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private BigDecimal size;
    @Enumerated(EnumType.STRING)
    private StarSystemType type;
    @OneToMany(mappedBy = "system", cascade = CascadeType.ALL)
    private Set<Star> stars;
    @OneToMany(mappedBy = "system", cascade = CascadeType.ALL)
    private Set<Planet> planets;
    @OneToMany(mappedBy = "system", cascade = CascadeType.ALL)
    private Set<SmallSpaceBody> smallSpaceBodies;
    @Enumerated(EnumType.STRING)
    private Galaxy galaxy;
}
