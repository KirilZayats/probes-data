package by.javacourse.probes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;


@MappedSuperclass
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SpaceObject extends BaseEntity<Long> {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private BigDecimal mass;
    private BigDecimal diameter;
    @Enumerated(EnumType.STRING)
    protected TypeOfSpaceObject objectType;
    @ManyToOne(cascade = CascadeType.ALL)
    private StarSystem system = new StarSystem();



}
