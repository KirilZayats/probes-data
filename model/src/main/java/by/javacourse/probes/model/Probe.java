package by.javacourse.probes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Set;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Probe extends BaseEntity<String> {
    @Id
    private String id;
    private String name;
    private String size;
    @Enumerated(EnumType.STRING)
    private Equipment classOfEquipment;
    private String currentLocation;
    @UpdateTimestamp
    private OffsetDateTime lastContact;
    @OneToMany(mappedBy = "probe", cascade = CascadeType.ALL)
    private Set<ProbeEquipment> equipment;
    @ManyToMany(fetch = FetchType.LAZY)
    private Set<ResearchObject> researchObjects;
    @Transient
    private StringBuilder data;
}
