package by.javacourse.probes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Set;

@Entity
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResearchObject extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Enumerated(EnumType.STRING)
    private TypeOfSpaceObject typeOfSpaceObject;
    private Integer numberOfResearchProbes;
    @Enumerated(EnumType.STRING)
    private Galaxy galaxy;
    @UpdateTimestamp
    private OffsetDateTime dataUpdateTime;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "researchObjects")
    private Set<Probe> lastDataUpdateProbes;
}
