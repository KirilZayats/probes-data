package by.javacourse.probes.model;

public enum Equipment {
    S_class,
    A_class,
    B_class,
    C_class,
    D_class
}
